Rakenduse käivitamiseks järgida vastava pdf'i juhendeid. Kui käivitada Eclipse's, järgida "Juhis-Eclipse.pdf" samme, kui käivitada Intellij's, järgida "Juhis - Intellij IDEA Community.pdf" juhiseid. Erinevused on veebibrowseris lõpuks avanev vaade, appi nimi on dentistapp (mitte prooviylesanne) ning olenevalt Eclipse'i (või Intellij) versioonidest mõned nupunimed (Apply and Close Ok asemel).

Järgnevalt on toodud kirjutatud kasutusjuhend, mis on toodud ka rakenduse esimesel veebilehel.

{{{Kasutusjuhend}}}
Vaikimisi on andmebaasi sisestatud suvalised andmed. Juhul, kui automaatset andmebaasi täitmist ei soovi, tuleb kustutada fail prooviylesanne\src\main\resources\data.sql.

Menüüribal toodud lingid suunavad vastavalt arsti lisamise, registreeringute vaatamise (ja otsimise) ning registreeringu tegemise lehele.

{{Menüünupp: Lisa arst}}
Lehel on toodud hetkel salvestatud arstide nimed. Arste saab ka lisada, kuid tuleb meeles pidada, et samanimelisi arste ei saa olla. Hetkeseisuga on suurtähestatud nimed väiketähestatud nimedest erinevad (ehk dracula ja Dracula on erinevad nimed).

Arsti lisamisel on piiranguks vaid nime pikkus: minimaalselt 1 täht ning maksimaalselt 50. Antud piirangut saab muuta lähtekoodis Dentist.java ja DentistDTO.java klassides.

{{Menüünupp: Registreeringud}}
Lehekülg koosneb salvestatud registreeringute listist, otsingust ning iga registreeringu juures olevatest detailvaate nuppudest.

{Otsing}
Otsida saab igat parameetrit täpsustades või täpsustades vaid soovituid. Arsti nime saab valida salvestatud arstinimede hulgast või lahtri tühjaks jätta (valides valiku nimega "---VALI---". Jättes kõik lahtrid tühjaks kuvatakse kõik registreeringud.

Visiidi kuupäev tuleb kirjutada formaadis "päeva-number.kuu-number.aasta-number" vastasel juhul teavitatakse kasutajat veateatega. Tasub tähele panna, et sisestatud kuupäev kujul "12.12.12" tõlgendatakse kuupäevaks "12.12.0012".

Lahter Peale kella kuvab visiidid, mille algus on sama või hiljem kui sisestatud kellaaeg. Sisestatud kellaaeg tuleb korrektselt sisestada, fomaadis "tunnid:minutid". Valesti sisestatud kellaaeg tekitab veateate.

Lahter Enne kella kuvab visiidid, mille algusaeg on enne või sama kui sisestatud kellaaeg. Sisestuse formaat sama, mis eelmisel lahtril.

{Registreeringute list}
Vaikimisi on kuvatud kõik registreeringud. Iga rea alguses olev "Detailvaade" nupp suunab kasutaja antud registreeringu detailvaatele, kus saab registreeringut muuta või kustutada.

{Detailvaade}
Detailvaates saab muuta kõiki lahtreid või vastava registreeringu kustutada. Juhul kui tahetakse salvestada vales formaadis lahtrit, suunatakse kasutaja antud detailvaatesse tagasi, ilma muudatusi salvestamata.

Vajutades "Kustuta" nuppu, suunatakse kasutaja tagasi registreeringute üldvaatesse ning vastav registreering kustutatakse andmebaasist.

{{Menüünupp: Broneeri}}
Lehel kuvatakse vorm, kus saab valida arsti nime, visiidi kuupäeva ja visiidi kellaaja. Hetkeseisuga tuleb kellaaeg ja kuupäev õiges formaadis sisestada ning visiidi vaikimisi kestvus on 1 tund. Seega ei saa broneerida visiiti, mis kattub teiste visiitidega tunniajases vahemikus (nii enne kui pärast).

Visiiti saab broneerida andmebaasi salvestatud arstide juurde ning arsti nimi tuleb valida ripp-menüüst. Kui soovitud arsti nimekirjast puudub, saab lahtri kõrval asuva "+" nupu kaudu liikuda lehele, "Lisa arst".

Valesti sisestatud kuupäeva ja kellaaja puhul ilmub veateade. Veateade ilmub ka juhul, kui registreeritud visiit kattub varasemalt salvestatud visiidiga. Visiidi vaikimisi kestvus on salvestatud DentistVisitService.java klassis.