package com.cgi.dentistapp.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.cgi.dentistapp.dao.entity.Dentist;

@Repository
public class DentistDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(Dentist dentist) throws Exception {
        entityManager.persist(dentist);
    }

    public List<Dentist> getAllDentists() {
        return entityManager.createQuery("SELECT e FROM Dentist e").getResultList();
    }
}
