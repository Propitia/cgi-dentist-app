package com.cgi.dentistapp.dao.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "dentist")
public class Dentist {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "name", nullable = false, length = 50, unique = true)
    private String name;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "dentist")
    private List<DentistVisitEntity> dentistVisitEntities;

    public Dentist() {
    }
    
    public Dentist(String name) {
        this.name = name;
    }

    public Dentist(Long id, String name, List<DentistVisitEntity> dentistVisitEntities) {
        this.id = id;
        this.name = name;
        this.dentistVisitEntities = dentistVisitEntities;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<DentistVisitEntity> getDentistVisitEntities() {
        return dentistVisitEntities;
    }

    public void setDentistVisitEntities(List<DentistVisitEntity> dentistVisitEntities) {
        this.dentistVisitEntities = dentistVisitEntities;
    }

}