package com.cgi.dentistapp.dao.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "dentist_visit")
public class DentistVisitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(name = "dentist_id", nullable = false)
    private Dentist dentist;

    @Column(name = "visit_date", nullable = false)
    private Date visitTime;

    @Column(name = "visit_time", nullable = false)
    private Date visitClock;

    public DentistVisitEntity() {

    }

    public DentistVisitEntity(Dentist dentist, Date visitTime, Date visitClock) {
        this.dentist = dentist;
        this.visitTime = visitTime;
        this.visitClock = visitClock;
    }
    
    public String getDentistName() {
        return this.dentist.getName();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Dentist getDentist() {
        return dentist;
    }

    public void setDentist(Dentist dentist) {
        this.dentist = dentist;
    }

    public Date getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }

    public Date getVisitClock() {
        return visitClock;
    }

    public void setVisitClock(Date visitClock) {
        this.visitClock = visitClock;
    }

}
