package com.cgi.dentistapp.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.cgi.dentistapp.dao.entity.Dentist;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.dto.DentistVisitSearchDTO;

import java.util.Date;
import java.util.List;

@Repository
public class DentistVisitDao {

    @PersistenceContext
    private EntityManager entityManager;

    public void create(DentistVisitEntity visit) {
        entityManager.persist(visit);
    }

    public void delete(Long id) {
        entityManager.remove(entityManager.getReference(DentistVisitEntity.class, id));
    }

    public void update(DentistVisitDTO dto) {
        DentistVisitEntity entity = entityManager.getReference(DentistVisitEntity.class, dto.getId());
        entity.setDentist(entityManager.getReference(Dentist.class, dto.getDentist()));
        entity.setVisitClock(dto.getVisitClock());
        entity.setVisitTime(dto.getVisitTime());
        entityManager.merge(entity);
    }

    public List<DentistVisitEntity> searchVisits(DentistVisitSearchDTO searchCommand) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<DentistVisitEntity> query = builder.createQuery(DentistVisitEntity.class);
        Root<DentistVisitEntity> r = query.from(DentistVisitEntity.class);

        Predicate predicate = builder.conjunction();

        if (searchCommand.getDentist() != null) {
            predicate = builder.and(predicate,
                    builder.equal(r.<Dentist>get("dentist").<Long>get("id"), searchCommand.getDentist()));
        }
        if (searchCommand.getVisitDate() != null) {
            predicate = builder.and(predicate,
                    builder.equal(r.<Date>get("visitTime"), searchCommand.getVisitDate()));
        }
        if (searchCommand.getVisitBefore() != null) {
            predicate = builder.and(predicate, builder.lessThanOrEqualTo(r.<Date>get("visitClock"), searchCommand.getVisitBefore()));
        }
        if (searchCommand.getVisitAfter() != null) {
            predicate = builder.and(predicate, builder.greaterThanOrEqualTo(r.<Date>get("visitClock"), searchCommand.getVisitAfter()));
        }

        query.where(predicate);

        List<DentistVisitEntity> result = entityManager.createQuery(query).getResultList();
        return result;
    }

    public List<DentistVisitEntity> getAllVisits() {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e").getResultList();
    }

    public DentistVisitEntity getDentistVisit(Long visitId) {
        return entityManager.getReference(DentistVisitEntity.class, visitId);
    }

}
