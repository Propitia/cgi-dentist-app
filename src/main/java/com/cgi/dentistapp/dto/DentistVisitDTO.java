package com.cgi.dentistapp.dto;

import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;

import java.util.Date;

/**
 * Created by serkp on 2.03.2017.
 */
public class DentistVisitDTO {

    private Long id;

    @NotNull
    private Long dentist;

    @NotNull
    @DateTimeFormat(pattern = "dd.MM.yyyy")
    private Date visitTime;

    @NotNull
    @DateTimeFormat(pattern = "HH:mm")
    private Date visitClock;

    public DentistVisitDTO() {
    }

    public DentistVisitDTO(Long id, Long dentist, Date visitTime, Date visitClock) {
        this.visitTime = visitTime;
        this.visitClock = visitClock;
        this.id = id;
        this.dentist = dentist;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDentist() {
        return dentist;
    }

    public void setDentist(Long dentist) {
        this.dentist = dentist;
    }

    public Date getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Date visitTime) {
        this.visitTime = visitTime;
    }

    public Date getVisitClock() {
        return visitClock;
    }

    public void setVisitClock(Date visitClock) {
        this.visitClock = visitClock;
    }

}
