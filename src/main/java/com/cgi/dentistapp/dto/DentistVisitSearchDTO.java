package com.cgi.dentistapp.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class DentistVisitSearchDTO {

    Long dentist;

    @DateTimeFormat(pattern = "dd.MM.yyyy")
    Date visitDate;

    @DateTimeFormat(pattern = "HH:mm")
    Date visitAfter;

    @DateTimeFormat(pattern = "HH:mm")
    Date visitBefore;

    public Long getDentist() {
        return dentist;
    }

    public void setDentist(Long dentist) {
        this.dentist = dentist;
    }

    public Date getVisitDate() {
        return visitDate;
    }

    public void setVisitDate(Date visitDate) {
        this.visitDate = visitDate;
    }

    public Date getVisitAfter() {
        return visitAfter;
    }

    public void setVisitAfter(Date visitAfter) {
        this.visitAfter = visitAfter;
    }

    public Date getVisitBefore() {
        return visitBefore;
    }

    public void setVisitBefore(Date visitBefore) {
        this.visitBefore = visitBefore;
    }

}
