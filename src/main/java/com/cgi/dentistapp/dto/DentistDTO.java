package com.cgi.dentistapp.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class DentistDTO {

    @NotNull
    @Size(min = 1, max = 50)
    String name;

    public DentistDTO() {
    }

    public DentistDTO(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
