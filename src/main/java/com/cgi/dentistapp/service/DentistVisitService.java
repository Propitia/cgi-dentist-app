package com.cgi.dentistapp.service;

import java.util.Calendar;
import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.Dentist;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.dto.DentistVisitSearchDTO;

@Service
@Transactional
public class DentistVisitService {
    private static final Integer RESERVATION_LENGTH_HOURS = 1;

    @Autowired
    private DentistVisitDao dentistVisitDao;

    @Autowired
    private EntityManager entityManager;

    public Boolean addVisit(DentistVisitDTO dentistVisitDTO) {
        if (!checkAvailability(dentistVisitDTO)) {
            return false;
        }
        DentistVisitEntity visit = new DentistVisitEntity(
                entityManager.getReference(Dentist.class, dentistVisitDTO.getDentist()), dentistVisitDTO.getVisitTime(),
                dentistVisitDTO.getVisitClock());
        dentistVisitDao.create(visit);
        return true;
    }

    private boolean checkAvailability(DentistVisitDTO dentistVisitDTO) {
        //this setting of times could be avoided if the database had start and end times
        DentistVisitSearchDTO searchDto = new DentistVisitSearchDTO();
        searchDto.setDentist(dentistVisitDTO.getDentist());
        searchDto.setVisitDate(dentistVisitDTO.getVisitTime());
        Calendar cal = Calendar.getInstance();
        cal.setTime(dentistVisitDTO.getVisitClock());
        //first set 59 minutes before to see, if there is an appointment before
        cal.add(Calendar.HOUR_OF_DAY, -RESERVATION_LENGTH_HOURS);
        cal.add(Calendar.MINUTE, 1);
        searchDto.setVisitAfter(cal.getTime());
        //then add 2 hours to see if 59 minutes after the appointment is free
        cal.add(Calendar.HOUR_OF_DAY, RESERVATION_LENGTH_HOURS * 2);
        cal.add(Calendar.MINUTE, -2);
        searchDto.setVisitBefore(cal.getTime());
        searchDto.setDentist(dentistVisitDTO.getDentist());
        List<DentistVisitEntity> result = dentistVisitDao.searchVisits(searchDto);
        return result.isEmpty();
    }

    public void deleteVisit(Long dentistVisitId) {
        dentistVisitDao.delete(dentistVisitId);
    }

    public List<DentistVisitEntity> listVisits(DentistVisitSearchDTO searchCommand) {
        return dentistVisitDao.searchVisits(searchCommand);
    }

    public DentistVisitEntity getVisit(Long id) {
        List<DentistVisitEntity> allVisits = dentistVisitDao.getAllVisits();
        for (int i = 0; i < allVisits.size(); i++) {
            if (allVisits.get(i).getId().equals(id)) {
                return allVisits.get(i);
            }
        }
        return null;
    }

    public DentistVisitEntity getDentistVisit(Long visitId) {
        return dentistVisitDao.getDentistVisit(visitId);
    }

    public DentistVisitDTO getDentistVisitDto(Long visitId) {
        DentistVisitEntity entity = getDentistVisit(visitId);
        return new DentistVisitDTO(entity.getId(), entity.getDentist().getId(), entity.getVisitTime(),
                entity.getVisitClock());
    }

    public void edit(DentistVisitDTO dto) {
        dentistVisitDao.update(dto);
    }

}
