package com.cgi.dentistapp.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistDao;
import com.cgi.dentistapp.dao.entity.Dentist;

@Service
@Transactional
public class DentistService {

    @Autowired
    private DentistDao dentistDao;

    public void saveDentist(String name) throws Exception {
        Dentist dentist = new Dentist(name);
        dentistDao.create(dentist);
    }

    public List<Dentist> listDentists() {
        return dentistDao.getAllDentists();
    }

}
