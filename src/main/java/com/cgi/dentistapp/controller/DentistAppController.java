package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.dao.entity.Dentist;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistDTO;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.dto.DentistVisitSearchDTO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.cgi.dentistapp.service.DentistService;
import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.util.List;

import javax.validation.Valid;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistVisitService dentistVisitService;

    @Autowired
    private DentistService dentistService;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
        registry.addViewController("/lisatud").setViewName("lisatud");
    }

    @GetMapping("/")
    public String showIndex() {
        return "index";
    }

    @PostMapping("/reception")
    public String postRegisterForm(@Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "form";
        }
        if (!dentistVisitService.addVisit(dentistVisitDTO)) {
            model.addAttribute("availability", false);
            //this should be refactored under DentistVisitDTO but I have no time
            List<Dentist> dentists = dentistService.listDentists();
            model.addAttribute("dentists", dentists);
            return "form";
        }
        return "redirect:/results";
    }

    @GetMapping("/reception")
    public String reservation(DentistVisitDTO dentistVisitDTO, Model model) {
        //this should be refactored under DentistVisitDTO but I have no time
        List<Dentist> dentists = dentistService.listDentists();
        model.addAttribute("dentists", dentists);
        return "reception";
    }

    @GetMapping("/addDentist")
    public String addDentist(DentistDTO dentistDTO, Model model) {
        //this should be refactored under DentistVisitDTO but I have no time
        List<Dentist> dentists = dentistService.listDentists();
        model.addAttribute("dentists", dentists);
        return "addDentist";
    }

    @PostMapping("/addDentist")
    public String saveDentist(@Valid DentistDTO dentistDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return "addDentist";
        }
        
        // exception arises, if one tries to add a dentist with a name, that is already present in the database (case not ignored) 
        try {
            dentistService.saveDentist(dentistDTO.getName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return "redirect:/addDentist";
    }

    @GetMapping("/list")
    public String list(Model model, @Valid DentistVisitSearchDTO searchCommand, BindingResult bindingResult) {
        //this should be refactored under DentistVisitDTO but I have no time
        List<Dentist> dentists = dentistService.listDentists();
        model.addAttribute("dentists", dentists);
        List<DentistVisitEntity> visits = dentistVisitService.listVisits(searchCommand);
        model.addAttribute("visits", visits);
        return "list";
    }

    @GetMapping("/view/{visitId:[\\d]+}")
    public String view(Model model, @PathVariable Long visitId, DentistVisitDTO dentistVisitDTO) {
        //this should be refactored under DentistVisitDTO but I have no time
        List<Dentist> dentists = dentistService.listDentists();
        model.addAttribute("dentists", dentists);
        model.addAttribute("visit", dentistVisitService.getDentistVisitDto(visitId));
        return "view";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "action=save")
    public String save(DentistVisitDTO dentistVisitDTO, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
//            if one has tried to modify the entity with erroneous data, one is redirected to the detail view 
            return "redirect:/view/" + dentistVisitDTO.getId();
        }
        dentistVisitService.edit(dentistVisitDTO);
        return "redirect:/list";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST, params = "action=delete")
    public String delete(DentistVisitDTO dentistVisitDTO) {
        dentistVisitService.deleteVisit(dentistVisitDTO.getId());
        return "redirect:/list";
    }
}
